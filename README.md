# NCBI_SRA_TO_EBI_FASTQ #
This python script will download .fastq/.fq files from the EBI (http://www.ebi.ac.uk/) using any ftp link of the NCBI (https://www.ncbi.nlm.nih.gov/) containing .sra (Sequence Read Archives file) files.
This is, to prevent the need of using the poorly documented fastq-dump (https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=toolkit_doc&f=fastq-dump) command line tool and understanding all it's parameters. Also to prevent unneccesary conversion and waste CPU cycles.

TLDR; The EBI converts & saves .sra files to .fastq files. Why download .sra's and convert them manually?

This program can also be used to download other links automatically (and other metadata), it will however, not go deeper with geoquery if samples are specified, so only links available on the given page. This script is experimental, please report issues on the issue tracker if you find any.

# LICENSE

Copyright (c) [2018] [Casper Peters and Robert Warmerdam] under the MIT license.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# Usage (as of v1.0);
(EXAMPLE)

`python3 ncbi_sra_to_ebi_fastq_cli.py --url 'ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByStudy/sra/SRP/SRP072' --output /data/somefolder/myfastqoutput`

(EXAMPLE with metadata)

`python3 ncbi_sra_to_ebi_fastq_cli.py --metaurl 'https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE79819' --output /data/somefolder/myfastqoutput`

to get a full comprehensive list of commands, type --help;

```
username@linuxcomputer:~$ ./ncbi_sra_to_ebi_fastq_cli.py --help
usage: ncbi_sra_to_ebi_fastq_cli.py [-h] [--metaurl METAURL | --url URL]
                                    [--output OUTPUT] [--no_decompress]
                                    [--nodel_gz] [--no_queries]
                                    [--non_verbose] [--less_verbose]
                                    [--only_meta]

This python script will download .fastq/.fq files from the EBI
(http://www.ebi.ac.uk/) using any ftp link of the NCBI
(https://www.ncbi.nlm.nih.gov/) containing .sra (Sequence Read Archives file)
files. This is, to prevent the need of using the poorly documented fastq-dump
(https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=toolkit_doc&f=fastq-
dump) command line tool and understanding all it's parameters / having to
convert everything and waste cpu cycles. Works for both paired-end and non-
paired data, will download metadata as well (if specified).

optional arguments:
  -h, --help         show this help message and exit
  --metaurl METAURL  Url for downloading metadata, will exportUseful info in a
                     meta.html file, openable in a browser. This will also
                     scan the page for ftp links and download any sra files
                     that can be found. Metadata will have no prompt for
                     downloading!
  --url URL          Ftp url for NCBI that contains the .sra files. This will
                     download only raw files with no metadata. The url can
                     point to a bunch of folders, the script will look within
                     these for the .sra files anyway.
  --output OUTPUT    Give the output folder to which to save the fastq files.
                     If not given, output folder will be created at run
                     location.
  --no_decompress    For leaving downloaded files compressed.
  --nodel_gz         Omits deleting the .fastq.gz's after decompression.
  --no_queries       For omitting querries before downloading
  --non_verbose      For omitting general output of program.
  --less_verbose     For omitting most output of program.
  --only_meta        For only downloading metadata and skipping download of
                     EBI fastq's
```

This is an early version of the script, if you run into any problems running this script, please add the issue(s) to the issue tracker.